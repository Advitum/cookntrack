# CookNTrack Version 3.0

CookNTrack bindet ein Cookie-Banner und einen Einstellungs-Dialog für den Nutzer ein. So können Vorgaben nach DSGVO einfach abgedeckt werden.

## Einbindung

```
CookNTrack.init();
```

Die Funktion `init` startet CookNTrack. Bei Bedarf kann als Parameter ein Objekt von Einstellungen übergeben werden.

Aktuell bringt CookNTrack Standard-Implementierungen für Google Analytics und den Google Tag Manager mit:

```
CookNTrack.init({
	defaultServices: {
		googleTagManager: 'XXXXX' // Tracking-Code
	}
});
```

oder

```
CookNTrack.init({
	defaultServices: {
		googleAnalytics: 'XXXXX' // Tracking-Code
	}
});
```

Bei Bedarf können weitere Services implementiert werden:

```
CookNTrack.init({
	services: {
		maps: {
			title: 'Titel für den Einstellungs-Dialog',
			description: 'Beschreibung für den Einstellungs-Dialog',
			url: 'URL-TO-JS',
			callback: function() {
				
			}
		}
	}
});
```

Titel und Beschreibung werden im Einstellungs-Dialog angezeigt. URL und Callback werden nur geladen bzw. ausgeführt, nachdem der Nutzer den Dienst (oder alle Dienste) erlaubt. Ist eine URL definiert, wird erst die URL geladen und danach das Callback ausgeführt.

Anhand der registrierten Services zeigt CookNTrack unterschiedliche Buttons. Sind keine Services registriert, werden nur die Buttons "Einstellungen" und "OK" gezeigt. Sobald es mindestens einen Service gibt, werden die Buttons "Einstellungen", "Verbieten" und "Erlauben" gezeigt.

Die Texte und Templates lassen sich über die Optionen `templates` und `strings` überschreiben. Siehe dazu die Standard-Optionen im Quellcode.

Über die Methode `preferences` kann der Einstellungs-Dialog jederzeit geöffnet werden. Das ist zum Beispiel auf der Seite "Datenschutz" wichtig:

```
CookNTrack.preferences();
```

Über die Methode `enableService` kann ein Dienst manuell aktiviert werden. Das ist zum Beispiel nötig, wenn ein Nutzer auf einer ausgeblendeten Karte explizit die Erlaubnis zum Laden der Karte gibt.

```
CookNTrack.enableService(serviceName);
```

## Beispiele

Einfaches Tracking über Google Analytics. Getrackt wird erst, nachdem der Nutzer auf Erlauben geklickt hat.

```
CookNTrack.init({
	defaultServices: {
		googleAnalytics: 'XXXXX' // Tracking-Code
	}
});
```

Opt-Out-Tracking über Google Analytics. Getrackt wird sofort, der Nutzer kann deaktivieren. Zusätzlich werden die Buttons angepasst, um den "Ablehnen"-Button zu verstecken. So kann der Nutzer nur unter "Einstellungen" deaktivieren.

```
CookNTrack.init({
	defaultServices: {
		googleAnalytics: 'XXXXX' // Tracking-Code
	},
	defaultBehavior: 'enable',
	templates: {
		buttons: '{preferences}{close}'
	}
});
```