/**
 * CookNTrack: Cookie notice and Google Tracking made simple
 * Version: 3.0.4, 2024/01/02
 * Author: Lars Ebert-Harlaar, Advitum.de
 * Web: https://advitum.de
 */
(function(window) {
	/* Initializes CookNTrack and shows cookie banner on demand */
	function CookNTrack(options) {
		this.options = CookNTrack.deepMerge(
			CookNTrack.defaultOptions,
			options || {}
		);
		
		// Storing all registered services
		this.services = {};
		
		for(var key in this.options.services) {
			if(typeof this.options.services[key].title == 'undefined') {
				continue;
			}
			if(typeof this.options.services[key].description == 'undefined') {
				continue;
			}
			if(typeof this.options.services[key].url == 'undefined'
				&& typeof this.options.services[key].callback == 'undefined'
			) {
				continue;
			}
			
			this.services[key] = this.options.services[key];
			this.services[key].initialized = false;
		}
		
		if(this.options.defaultServices.googleAnalytics !== null) {
			this.services.googleAnalytics = {
				title: this.options.strings.googleAnalyticsCookies.title,
				description: this.options.strings.googleAnalyticsCookies.description,
				callback: (function() {
					window.dataLayer = window.dataLayer || [];
					window.gtag = function(){dataLayer.push(arguments);};
					gtag('js', new Date());
					gtag('config', this.options.defaultServices.googleAnalytics, {'anonymize_ip': true});
					
					var externalScript = document.createElement('script');
					externalScript.setAttribute('src', 'https://www.googletagmanager.com/gtag/js?id=' + this.options.defaultServices.googleAnalytics);
					externalScript.setAttribute('defer', 'defer');
					document.getElementsByTagName('head')[0].appendChild(externalScript);
				}).bind(this),
				initialized: false
			};
		}
		
		if(this.options.defaultServices.googleTagManager !== null) {
			this.services.googleTagManager = {
				title: this.options.strings.googleAnalyticsCookies.title,
				description: this.options.strings.googleAnalyticsCookies.description,
				callback: (function() {
					window.dataLayer = window.dataLayer || [];
					window.dataLayer.push({
						'gtm.start': new Date().getTime(),
						event: 'gtm.js'
					});
					
					var externalScript = document.createElement('script');
					externalScript.setAttribute('src', 'https://www.googletagmanager.com/gtm.js?id=' + this.options.defaultServices.googleTagManager);
					externalScript.setAttribute('defer', 'defer');
					document.getElementsByTagName('head')[0].appendChild(externalScript);
				}).bind(this),
				initialized: false
			};
		}
		
		this.preferences = this.loadPreferences();
		
		var doNotTrackEnabled = CookNTrack.checkDoNotTrack();
		
		for(var key in this.services) {
			if(typeof this.preferences.services[key] == 'undefined') {
				// This services is new, user has to be informed
				this.preferences.closed = false;
				
				this.preferences.services[key] = doNotTrackEnabled ? 'disable' : this.options.defaultBehavior;
			}
			
			if(this.preferences.services[key] === 'enable') {
				this.initializeService(key);
			}
		}
		
		// We only show the cookie notice if a user has not yet closed it
		if(!this.preferences.closed) {
			this.addCookieNotice();
		}
	}
	
	CookNTrack.defaultOptions = {
		// (string) Prefix used in class and id attributes
		idClassPrefix: 'cookNTrack',
		
		// Localstorage Key used for storing user choices
		storageKey: 'cookntrack_preferences',
		
		// Default behavior. Can either be "enable" or "disable".
		defaultBehavior: 'disable',
		
		// Default implementations of different services.
		// For each value set, CookNTrack will load its default implementation.
		defaultServices: {
			// Google Analytics Tracking ID
			googleAnalytics: null,
			
			// Google Tag Manager ID
			googleTagManager: null,
		},
		
		// Templates used for rendering all elements
		templates: {
			cookieNotice: '<div class="{idPrefix}_message">{message}</div><div class="{idPrefix}_buttons">{buttons}</div></div>',
			cookiePreferences: '<div class="{idPrefix}_preferences_title">{title}</div><div class="{idPrefix}_preferences_intro">{intro}</div><div class="{idPrefix}_preferences_list">{list}</div><div class="{idPrefix}_buttons">{buttons}</div>',
			service: '<div class="{idPrefix}_service"><div class="{idPrefix}_service_title">{title}</div><div class="{idPrefix}_service_description">{description}</div><div class="{idPrefix}_service_checkbox">{checkbox}</div></div>',
			
			// If no buttons template is set, CookNTrack chooses the buttons based on all other settings.
			// Available buttons: {preferences}, {close}, {accept}, {decline}
			buttons: null
		},
		
		// Default strings for easy translating
		strings: {
			cookieNotice: {
				message: 'Diese Webseite verwendet Cookies.<br /><a href="/datenschutz">Datenschutz</a>'
			},
			cookiePreferences: {
				title: 'Cookie-Einstellungen',
				intro: 'Hier können Sie festlegen, welche Arten von Cookies Sie erlauben wollen.'
			},
			functionalCookies: {
				title: 'Funktionale Cookies',
				description: 'Diese Cookies sind für die Verwendung zwingend nötig und werden nur für technische Zwecke genutzt. Diese Cookies enthalten keine personenbezogenen Daten und werden nicht zu Analyse-Zwecken verwendet.'
			},
			googleAnalyticsCookies: {
				title: 'Tracking über Google Analytics oder den Google Tag Manager',
				description: 'Diese Cookies nutzen wir, um das Surf-Verhalten unserer Nutzer zu analysieren. Die Daten werden anonymisiert an Google übertragen.'
			},
			preferences: 'Einstellungen',
			disable: 'Ablehnen',
			enable: 'Erlauben',
			close: 'OK'
		}
	};
	
	CookNTrack.singletonInstance = null;
	
	CookNTrack.deepMerge = function(a, b) {
		var result = Object.assign({}, a);
		for(var key in b) {
			if(typeof b[key] == 'object' && typeof result[key] == 'object') {
				result[key] = CookNTrack.deepMerge(result[key], b[key]);
				continue;
			}
			result[key] = b[key];
		}
		return result;
	};
	
	CookNTrack.checkDoNotTrack = function() {
		if(window.doNotTrack || false) {
			return true;
		}
		if((navigator.doNotTrack || 'no') === 'yes') {
			return true;
		}
		if(navigator.msDoNotTrack || false) {
			return true;
		}
		if('external' in window
			&& 'msTrackingProtectionEnabled' in window.external
			&& window.external.msTrackingProtectionEnabled()
		) {
			return true;
		}
		
		return false;
	};
	
	CookNTrack.prototype.replacePlaceholders = function(string) {
		string = string.replace(/{idPrefix}/g, this.options.idClassPrefix);
		
		return string;
	};
	
	CookNTrack.prototype.buildCookieNoticeHTML = function() {
		var string = this.options.templates.cookieNotice
		string = this.replacePlaceholders(string);
		
		string = string.replace(/{message}/g, this.options.strings.cookieNotice.message);
		string = string.replace(/{buttons}/g, (function() {
			var buttonTemplate = this.options.templates.buttons;
			if(!buttonTemplate) {
				if(Object.keys(this.services).length) {
					buttonTemplate = '{preferences}{disable}{enable}';
				} else {
					buttonTemplate = '{preferences}{close}';
				}
			}
			
			buttonTemplate = buttonTemplate.replace(/{preferences}/g, '<button class="' + this.options.idClassPrefix + '_button_preferences">' + this.options.strings.preferences + '</button>');
			buttonTemplate = buttonTemplate.replace(/{close}/g, '<button class="' + this.options.idClassPrefix + '_button_close">' + this.options.strings.close + '</button>');
			buttonTemplate = buttonTemplate.replace(/{disable}/g, '<button class="' + this.options.idClassPrefix + '_button_disable">' + this.options.strings.disable + '</button>');
			buttonTemplate = buttonTemplate.replace(/{enable}/g, '<button class="' + this.options.idClassPrefix + '_button_enable">' + this.options.strings.enable + '</button>');
			
			return buttonTemplate;
		}).bind(this));
		
		return string;
	};
	
	CookNTrack.prototype.addCookieNotice = function() {
		if(this.cookieNoticeElement || false) {
			this.cookieNoticeElement.remove();
			this.cookieNoticeElement = false;
		}
		
		this.cookieNoticeElement = document.createElement('div');
		this.cookieNoticeElement.setAttribute('id', this.options.idClassPrefix);
		document.body.appendChild(this.cookieNoticeElement);
		
		this.cookieNoticeElement.innerHTML = this.buildCookieNoticeHTML();
		
		this.cookieNoticeElement.addEventListener('click', (function(event) {
			if(event.target.classList.contains(this.options.idClassPrefix + '_button_preferences')) {
				event.preventDefault();
				
				this.addCookiePreferencesOverlay();
			}
			if(event.target.classList.contains(this.options.idClassPrefix + '_button_close')) {
				event.preventDefault();
				
				this.preferences.closed = true;
				this.savePreferences(this.preferences);
				
				if(this.cookiePreferencesOverlayElement || false) {
					this.cookiePreferencesOverlayElement.remove();
					this.cookiePreferencesOverlayElement = false;
				}
				
				if(this.cookieNoticeElement || false) {
					this.cookieNoticeElement.remove();
					this.cookieNoticeElement = false;
				}
			}
			if(event.target.classList.contains(this.options.idClassPrefix + '_button_disable')) {
				event.preventDefault();
				
				for(var key in this.services) {
					this.preferences.services[key] = 'disable';
				}
				
				this.preferences.closed = true;
				this.savePreferences(this.preferences);
				
				if(this.cookiePreferencesOverlayElement || false) {
					this.cookiePreferencesOverlayElement.remove();
					this.cookiePreferencesOverlayElement = false;
				}
				
				if(this.cookieNoticeElement || false) {
					this.cookieNoticeElement.remove();
					this.cookieNoticeElement = false;
				}
			}
			if(event.target.classList.contains(this.options.idClassPrefix + '_button_enable')) {
				event.preventDefault();
				
				for(var key in this.services) {
					this.preferences.services[key] = 'enable';
					this.initializeService(key);
				}
				
				this.preferences.closed = true;
				this.savePreferences(this.preferences);
				
				if(this.cookiePreferencesOverlayElement || false) {
					this.cookiePreferencesOverlayElement.remove();
					this.cookiePreferencesOverlayElement = false;
				}
				
				if(this.cookieNoticeElement || false) {
					this.cookieNoticeElement.remove();
					this.cookieNoticeElement = false;
				}
			}
		}).bind(this));
	};
	
	CookNTrack.prototype.buildCookiePreferencesOverlayHTML = function() {
		var string = this.options.templates.cookiePreferences;
		
		string = this.replacePlaceholders(string);
		
		string = string.replace(/{title}/g, this.options.strings.cookiePreferences.title);
		string = string.replace(/{intro}/g, this.options.strings.cookiePreferences.intro);
		string = string.replace(/{list}/g, (function() {
			var list = '';
			
			var service = this.options.templates.service;
			
			service = this.replacePlaceholders(service);
			service = service.replace(/{title}/g, this.options.strings.functionalCookies.title);
			service = service.replace(/{description}/g, this.options.strings.functionalCookies.description);
			service = service.replace(/{checkbox}/g, '<input type="checkbox" readonly checked />');
			
			list += service;
			
			for(var key in this.services) {
				var service = this.options.templates.service;
				
				service = this.replacePlaceholders(service);
				service = service.replace(/{title}/g, this.services[key].title);
				service = service.replace(/{description}/g, this.services[key].description);
				service = service.replace(/{checkbox}/g, '<input type="checkbox" name="' + key + '"' + (
					this.preferences.services[key] === 'enable' ? ' checked' : ''
				) + ' />');
				
				list += service;
			}
			return list;
		}).bind(this));
		string = string.replace(/{buttons}/g, '<button class="' + this.options.idClassPrefix + '_button_close">' + this.options.strings.close + '</button>');
		
		return string;
	};
	
	CookNTrack.prototype.addCookiePreferencesOverlay = function() {
		if(this.cookieNoticeElement || false) {
			this.cookieNoticeElement.remove();
			this.cookieNoticeElement = false;
		}
		
		if(this.cookiePreferencesOverlayElement || false) {
			this.cookiePreferencesOverlayElement.remove();
			this.cookiePreferencesOverlayElement = false;
		}
		
		this.cookiePreferencesOverlayElement = document.createElement('div');
		this.cookiePreferencesOverlayElement.setAttribute('id', this.options.idClassPrefix + '_preferences');
		document.body.appendChild(this.cookiePreferencesOverlayElement);
		
		this.cookiePreferencesOverlayElement.innerHTML = this.buildCookiePreferencesOverlayHTML();
		
		this.cookiePreferencesOverlayElement.addEventListener('click', (function(event) {
			if(event.target.matches('input[type="checkbox"][readonly]')) {
				event.preventDefault();
				return false;
			}
			
			if(event.target.classList.contains(this.options.idClassPrefix + '_button_close')) {
				event.preventDefault();
				
				this.preferences.closed = true;
				
				for(var key in this.services) {
					this.preferences.services[key] = 
						this.cookiePreferencesOverlayElement.querySelector('input[name="' + key + '"]').checked
						? 'enable'
						: 'disable'
					;
					
					if(this.preferences.services[key] === 'enable') {
						this.initializeService(key);
					}
				}
				
				this.savePreferences(this.preferences);
				
				if(this.cookiePreferencesOverlayElement || false) {
					this.cookiePreferencesOverlayElement.remove();
					this.cookiePreferencesOverlayElement = false;
				}
				
				if(this.cookieNoticeElement || false) {
					this.cookieNoticeElement.remove();
					this.cookieNoticeElement = false;
				}
			}
		}).bind(this));
	};
	
	CookNTrack.prototype.loadPreferences = function() {
		var preferences = {}
		try {
			preferences = JSON.parse(window.localStorage.getItem(this.options.storageKey)) || {};
		} catch(error) {
			preferences = {};
		}
		preferences = CookNTrack.deepMerge({
			closed: false,
			services: {}
		}, preferences);
		return preferences;
	};
	
	CookNTrack.prototype.savePreferences = function(preferences) {
		window.localStorage.setItem(this.options.storageKey, JSON.stringify(preferences));
	};
	
	CookNTrack.prototype.initializeService = function(key) {
		if(typeof this.services[key] == 'undefined') {
			return;
		}
		if(this.services[key].initialized) {
			return;
		}
		
		this.services[key].initialized = true;
		
		if(typeof this.services[key].url !== 'undefined') {
			var externalScript = document.createElement('script');
			externalScript.setAttribute('src', this.services[key].url);
			externalScript.setAttribute('defer', 'defer');
			
			if(typeof this.services[key].callback == 'function') {
				(function(externalScript, callback) {
					externalScript.onload = function() {
						callback();
					};
				})(externalScript, this.services[key].callback);
			}
			
			document.getElementsByTagName('head')[0].appendChild(externalScript);
		} else if(typeof this.services[key].callback == 'function') {
			this.services[key].callback();
		}
	}
	
	// Exposing public interface
	window.CookNTrack = {
		// initializes CookNTrack with the passed options
		init: function(options) {
			if(CookNTrack.singletonInstance) {
				return CookNTrack.singletonInstance;
			}
			
			CookNTrack.singletonInstance = new CookNTrack(options);
		},
		
		// Use this function to enable services manually, for example when the user
		// clicks "load map" on a map disclaimer.
		enableService: function(serviceName) {
			if(CookNTrack.singletonInstance) {
				if(typeof CookNTrack.singletonInstance.services[serviceName] != 'undefined') {
					CookNTrack.singletonInstance.preferences.services[serviceName] = 'enable';
					CookNTrack.singletonInstance.savePreferences(CookNTrack.singletonInstance.preferences);
					CookNTrack.singletonInstance.initializeService(serviceName);
				}
			}
		},
		
		// Trigger the preferences dialogue. Useful for the privacy page
		preferences: function() {
			if(CookNTrack.singletonInstance) {
				CookNTrack.singletonInstance.addCookiePreferencesOverlay();
			}
		},
	};
})(window);